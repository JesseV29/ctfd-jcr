# ENV Variables

There are 3 ENV variables that you **must** specify:

- SETUP_EVENT_NAME
- SETUP_EVENT_MODE
- SETUP_EVENT_ADMIN

There are 2 ENV variables that you **can** specify:

- SETUP_ACCOUNT_TYPE
- SETUP_EMAIL_ENABLED

## 1. SETUP_EVENT_NAME

The value of this ENV variable can be anything that you prefer.

## 2. SETUP_EVENT_MODE

The value of this ENV variable can be either `users` (challenges are played individually) or `teams` (challenges are played as a team).

## 3. SETUP_EVENT_ADMIN

The value of this ENV variable can only be an email address. This is where you specify who will be the administrator of your CTFd deployment (must be a valid email address)

## 4. SETUP_ACCOUNT_TYPE

The value of this ENV variable can be either 1 of the following: `ctfd`, `surf`, `both`.

- `ctfd`: users can register/login with CTFd accounts
- `surf`: users can login with SURFconext accounts
- `both`: a combination of `ctfd` and `surf`

There are [other ENV variables](../../custom_auth/readme/ENV_VARIABLES.md) that you **must** specify if this ENV variable is `surf` or `both`. If this ENV variable is unspecified or empty, the default value (`ctfd`) is used.

## 5. SETUP_EMAIL_ENABLED

The value of this ENV variable can be either `true` or `false`. This ENV variable cannot be `true` if the ENV variable SETUP_ACCOUNT_TYPE is be `surf`. If this ENV variable is unspecified or empty, the default value (`false`) is used.

There are 6 ENV variables that you **must** specify if this ENV variable is `true`:

- EMAIL_SERVER
- EMAIL_PORT
- EMAIL_PROTOCOL
- EMAIL_ADDRESS
- EMAIL_USERNAME
- EMAIL_PASSWORD

## 6. EMAIL_SERVER

The value of this ENV variable is the mail server address (must be a valid email address). This ENV variable cannot be empty if the ENV variable SETUP_EMAIL_ENABLED is `true`.

**Gmail** example: `smtp.gmail.com`

## 7. EMAIL_PORT

The value of this ENV variable is the mail server port (must be a whole number). This ENV variable cannot be empty if the ENV variable SETUP_EMAIL_ENABLED is `true`.

**Gmail** example: `587`

## 8. EMAIL_PROTOCOL

The value of this ENV variable can be either `ssl` or `tls`. This ENV variable cannot be empty if the ENV variable SETUP_EMAIL_ENABLED is `true`.

**Gmail** example: `tls`

## 9. EMAIL_ADDRESS

The value of this ENV variable is the email address the mails are sent from (must be a valid email address). This ENV variable cannot be empty if the ENV variable SETUP_EMAIL_ENABLED is `true`.

**Gmail** example: `mail@gmail.com`

## 10. EMAIL_USERNAME

The value of this ENV variable is the username of the mail server. This ENV variable cannot be empty if the ENV variable SETUP_EMAIL_ENABLED is `true`.

**Gmail** example: `mail@gmail.com`

## 11. EMAIL_PASSWORD

The value of this ENV variable is the password of the mail server. This ENV variable cannot be empty if the ENV variable SETUP_EMAIL_ENABLED is `true`.

**Gmail** example: `secret`
