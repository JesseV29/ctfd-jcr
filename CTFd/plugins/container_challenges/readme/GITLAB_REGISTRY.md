# Gitlab Registry

In order to upload container challenges to CTFd, all challenges must be stored in the [Gitlab registry](https://gitlab.com/hu-hc/jcr/challenges). Each challenge has its own Gitlab repository which must have the following files in order to work properly:

- `Image/` folder containing challenge assets (**irrelevant** for this documentation)
- `.gitlab-ci.yml` file containing CI pipeline (**relevant** for this documentation)
- `Dockerfile` file containing the challenge environment (**irrelevant** for this documentation)
- `compose.yml` file containing image location (**relevant** for this documentation)

## 1. `.gitlab-ci.yml`

```yml
image: alpine:latest

variables:
  DOCKER_TLS_CERTDIR: ""

stages:
  - build

build:
  stage: build
  image: "registry.gitlab.com/gitlab-org/cluster-integration/auto-build-image:v0.4.0"
  services:
    - docker:19.03.12-dind
  script:
    - |
      if [[ -z "$CI_COMMIT_TAG" ]]; then
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA}
      else
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_TAG}
      fi
    - /build/build.sh
  only:
    - master
  except:
    changes:
      - "README.md"
      - "compose.yml"
      - "docker-compose.yml"
      - "compose.yaml"
      - "docker-compose.yaml"
      - "challenge.yml"
      - "challenge.yaml"
```

This file will trigger the build of your challenge image **every** time you change the `Dockerfile` or the `Image/` directory. The contents of this file is the same for every challenge, because every challenge has roughly the same structure.

The built image versions are stored at: **<challenge_repository> ---> Packages & Registries ---> Container Registry**.

## 2. `compose.yml`

**NOTE**: This example is just showing how to refer to your challenge image location. It is **not** a complete compose file!

```yml
version: <MY_VERSION>

services:
   <MY_CHALLENGE>:
     container_name: <MY_CHALLENGE>
     image: registry.gitlab.com/hu-hc/jcr/challenges/<MY_CHALLENGE>:latest
...
```

This file will be uploaded to CTFd when creating a new container challenge. As you can see, it refers to the built image of your challenge, located in the JCR Gitlab registry. You can adjust this file for your challenge as you wish, as long as the image location is `registry.gitlab.com/hu-hc/jcr/challenges/<MY_CHALLENGE>:latest`
