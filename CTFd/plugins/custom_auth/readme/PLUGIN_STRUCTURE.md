# Plugin Structure

```text
CTFd
└── plugins
    └── custom_auth
        └── code
            └── confirm.py
            └── login.py
            └── register.py
            └── reset.py
            └── settings.py
            └── surfconext.py
            └── tools.py
        └── templates
            └── login_page_both.html
            └── login_page_ctfd.html
            └── login_page_surf.html
            └── register_page.html
            └── reset_page.html
            └── settings_page.html
        └── __init__.py
```

## `confirm.py`

This file contains the Python code for verifying registered CTFd accounts through email.

## `login.py`

This file contains the Python code for logging in CTFd accounts.

## `register.py`

This file contains the Python code for registering CTFd accounts.

## `reset.py`

This file contains the Python code for resetting the password of CTFd accounts through email.

## `settings.py`

This file contains the Python code for changing account settings.

## `surfconext.py`

This file contains the Python code for logging in through SURFconext.

## `tools.py`

This file contains the Python code for validating strong passwords.

## `login_page_both.html`

This file contains the HTML of the customized login page (overwrites the default CTFd login page). It contains both the CTFd login and the SURFconext login.

## `login_page_ctfd.html`

This file contains the HTML of the customized login page (overwrites the default CTFd login page). It contains only the CTFd login.

## `login_page_surf.html`

This file contains the HTML of the customized login page (overwrites the default CTFd login page). It contains only the SURFconext login.

## `register_page.html`

This file contains the HTML file of the customized register page (overwrites the default CTFd register page).

## `reset_page.html`

This file contains the HTML file of the customized password reset page (overwrites the default CTFd password reset page).

## `settings_page.html`

This file contains the HTML file of the customized settings page (overwrites the default CTFd settings page).

## `__init__.py`

This file is the CTFd plugin entrypoint that is called first when CTFd starts. It is the core file of the plugin.
